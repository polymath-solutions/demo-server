package main

import (
//	"encoding/json"
	"io/ioutil"
	"fmt"
	"log"
//	"net/http"
//	"net/url"
	"os"
//	"strings"

//	"golang.org/x/net/html"
        "gopkg.in/yaml.v2"
        "gopkg.in/russross/blackfriday.v2"
)

var data = `
a: Easy!
b:
  c: 2
  d: [3, 4]
`

type T struct {
        A string
        B struct {
                RenamedC int   `yaml:"c"`
                D        []int `yaml:",flow"`
        }
}

func main() {
        fmt.Println("Hello, world!")
       t := T{}
    
        err := yaml.Unmarshal([]byte(data), &t)
        if err != nil {
                log.Fatalf("error: %v", err)
        }
        fmt.Printf("--- t:\n%v\n\n", t)
    
        d, err := yaml.Marshal(&t)
        if err != nil {
                log.Fatalf("error: %v", err)
        }
        fmt.Printf("--- t dump:\n%s\n\n", string(d))
    
        m := make(map[interface{}]interface{})
    
        err = yaml.Unmarshal([]byte(data), &m)
        if err != nil {
                log.Fatalf("error: %v", err)
        }
        fmt.Printf("--- m:\n%v\n\n", m)
    
        d, err = yaml.Marshal(&m)
        if err != nil {
                log.Fatalf("error: %v", err)
        }
        fmt.Printf("--- m dump:\n%s\n\n", string(d))

	md := []byte(`Taken from here (https://www.example.com/example.html):`)
	html := blackfriday.Run(md)
	fmt.Println(string(html))

        var input []byte
//	var err error
	if input, err = ioutil.ReadFile("CppCoreGuidelines.md"); err != nil {
		fmt.Fprintln(os.Stderr, "Error reading from Stdin:", err)
		os.Exit(-1)
	}
        doc := blackfriday.Run(input)
        fmt.Println(string(doc))
}
