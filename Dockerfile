# Note: we set P in all phases, I don't seem to have a setting persist.
# I can see why this is, but still, bummer!

#--------------------------------------------------------------------
# Build Go binaries
#--------------------------------------------------------------------
FROM golang:1.9.2-alpine3.7 AS builder

ENV  P=/go/src/github.com/polymath-solutions/demo-server
WORKDIR $P

RUN apk add --no-cache git
RUN go get -u github.com/golang/dep/...

#RUN go get -d -v golang.org/x/net/html
#RUN go get -d gopkg.in/yaml.v2
#RUN go get -u gopkg.in/russross/blackfriday.v2

COPY server.go	.
#COPY Gopkg.toml .
#COPY Gopkg.lock .

RUN dep init
RUN dep ensure -v #-update
RUN cat Gopkg.lock

RUN echo $GOPATH

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o server .

#--------------------------------------------------------------------
# Build final image
#--------------------------------------------------------------------
FROM alpine:3.7
ENV  P=/go/src/github.com/polymath-solutions/demo-server
RUN apk --no-cache add ca-certificates

WORKDIR /root/

COPY --from=builder $P/server    .
COPY CppCoreGuidelines.md .
#COPY --from=builder /go/src/github.com/polymath-solutions/demo-server/server    .

CMD ["./server"]
